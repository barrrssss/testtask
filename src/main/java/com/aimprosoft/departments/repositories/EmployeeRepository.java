package com.aimprosoft.departments.repositories;

import com.aimprosoft.departments.models.Employee;

import java.util.List;

public interface EmployeeRepository {

    List<Employee> getDepartmentEmployees(Integer departmentId);

    Employee saveEmployee(Employee employee, Integer departmentId);

    Employee getEmployeeById(Integer id);

    void deleteById(Integer id);

    Employee getEmployeeByEmail(String email);
}
