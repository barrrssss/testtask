package com.aimprosoft.departments.controllers.commands.employee;

import com.aimprosoft.departments.controllers.commands.Command;
import com.aimprosoft.departments.services.EmployeeService;
import com.aimprosoft.departments.services.impl.EmployeeServiceImpl;

public abstract class EmployeeCommand implements Command {

    protected final EmployeeService employeeService;

    public EmployeeCommand() {
        this.employeeService = new EmployeeServiceImpl();
    }
}
