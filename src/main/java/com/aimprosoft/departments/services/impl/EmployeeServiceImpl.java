package com.aimprosoft.departments.services.impl;

import com.aimprosoft.departments.models.Employee;
import com.aimprosoft.departments.repositories.EmployeeRepository;
import com.aimprosoft.departments.repositories.impl.JdbcEmployeeRepository;
import com.aimprosoft.departments.services.EmployeeService;
import com.aimprosoft.departments.utils.exceptions.ConstraintViolationException;
import com.aimprosoft.departments.validation.OvalValidator;

import java.util.Collections;
import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final OvalValidator ovalValidator;

    public EmployeeServiceImpl() {
        this.employeeRepository = new JdbcEmployeeRepository();
        this.ovalValidator = new OvalValidator();
    }

    @Override
    public List<Employee> getDepartmentEmployees(Integer departmentId) {
        if (departmentId == null) return Collections.emptyList();
        return employeeRepository.getDepartmentEmployees(departmentId);
    }

    @Override
    public void saveEmployee(Employee employee, Integer departmentId) throws ConstraintViolationException {
        ovalValidator.validateEntity(employee);
        employeeRepository.saveEmployee(employee, departmentId);
    }

    @Override
    public Employee getEmployeeById(Integer id) {
        if (id == null) return null;
        return employeeRepository.getEmployeeById(id);
    }

    @Override
    public void deleteById(Integer id) {
        if (id == null) return;
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee getEmployeeByEmail(String email) {
        return employeeRepository.getEmployeeByEmail(email);
    }
}
