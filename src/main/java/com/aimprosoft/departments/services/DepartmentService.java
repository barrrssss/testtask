package com.aimprosoft.departments.services;

import com.aimprosoft.departments.models.Department;
import com.aimprosoft.departments.utils.exceptions.ConstraintViolationException;

import java.util.List;

public interface DepartmentService {

    List<Department> getAllDepartments();

    void saveDepartment(Department department) throws ConstraintViolationException;

    Department getDepartmentById(Integer id);

    void deleteById(Integer id);

    Department getDepartmentByName(String name);
}
