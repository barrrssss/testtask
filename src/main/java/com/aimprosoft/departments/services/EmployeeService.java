package com.aimprosoft.departments.services;

import com.aimprosoft.departments.models.Employee;
import com.aimprosoft.departments.utils.exceptions.ConstraintViolationException;

import java.util.List;

public interface EmployeeService {

    List<Employee> getDepartmentEmployees(Integer departmentId);

    void saveEmployee(Employee employee, Integer departmentId) throws ConstraintViolationException;

    Employee getEmployeeById(Integer id);

    void deleteById(Integer employeeId);

    Employee getEmployeeByEmail(String email);
}
