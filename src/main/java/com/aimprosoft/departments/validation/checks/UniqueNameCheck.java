package com.aimprosoft.departments.validation.checks;

import net.sf.oval.constraint.CheckWithCheck;
import com.aimprosoft.departments.models.Department;
import com.aimprosoft.departments.services.DepartmentService;
import com.aimprosoft.departments.services.impl.DepartmentServiceImpl;

public class UniqueNameCheck implements CheckWithCheck.SimpleCheck {

    private final DepartmentService departmentService;

    private UniqueNameCheck() {
        this.departmentService = new DepartmentServiceImpl();
    }

    @Override
    public boolean isSatisfied(Object validatedObject, Object value) {
        String name = (String) value;
        Department department = (Department) validatedObject;
        Department thatDepartment = departmentService.getDepartmentByName(name);
        return thatDepartment == null || thatDepartment.getId().equals(department.getId());
    }
}