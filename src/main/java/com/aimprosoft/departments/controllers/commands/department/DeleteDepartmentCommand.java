package com.aimprosoft.departments.controllers.commands.department;

import com.aimprosoft.departments.utils.WebUtil;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteDepartmentCommand extends DepartmentCommand {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer departmentId = WebUtil.parseIntParameterOrNull(req, "departmentId");
        departmentService.deleteById(departmentId);
        resp.sendRedirect("departments");
    }
}
