package com.aimprosoft.departments.utils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class WebUtil {
    public static Integer parseIntParameterOrNull(HttpServletRequest request, String parameterName) {
        String parameter = request.getParameter(parameterName);
        try {
            return Integer.valueOf(parameter);
        } catch (NumberFormatException e){
            return null;
        }
    }

    public static LocalDate parseDateParameter(HttpServletRequest request, String parameterName){
        String date = request.getParameter(parameterName);
        try {
            return LocalDate.parse(date);
        } catch (DateTimeParseException e) {
            return null;
        }
    }
}
