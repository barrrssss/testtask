package com.aimprosoft.departments.controllers.commands.department;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GetAllDepartmentsCommand extends DepartmentCommand {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("departments", departmentService.getAllDepartments());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("departmentsPage");
        requestDispatcher.forward(req, resp);
    }
}
