package com.aimprosoft.departments.controllers.commands.department;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aimprosoft.departments.models.Department;
import com.aimprosoft.departments.utils.WebUtil;
import com.aimprosoft.departments.utils.exceptions.ConstraintViolationException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class SaveDepartmentCommand extends DepartmentCommand {

    private static final Logger logger = LoggerFactory.getLogger(SaveDepartmentCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer departmentId = WebUtil.parseIntParameterOrNull(request, "departmentId");
        Department department = new Department();
        department.setId(departmentId);
        department.setName(request.getParameter("name"));
        try {
            departmentService.saveDepartment(department);
            response.sendRedirect("departments");
        } catch (ConstraintViolationException e){
            Map<String, String> violations = e.getConstraintViolations();
            logger.info("constraint violations {}", violations);
            request.setAttribute("violations", violations);
            request.setAttribute("department", department);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("departmentPage");
            requestDispatcher.forward(request, response);
        }
    }
}
