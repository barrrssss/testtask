package com.aimprosoft.departments.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aimprosoft.departments.controllers.commands.*;
import com.aimprosoft.departments.utils.exceptions.BadRequestException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FrontController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(FrontController.class);

    private final CommandFactory commandFactory = new CommandFactory();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();

        logger.debug("URI = {}", uri);

        try {
            Command command = commandFactory.getCommand(uri);
            command.execute(req, resp);
        } catch (BadRequestException e){
            resp.sendError(400, e.getMessage());
        }
    }
}
