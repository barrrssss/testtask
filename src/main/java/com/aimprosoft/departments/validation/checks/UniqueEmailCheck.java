package com.aimprosoft.departments.validation.checks;

import net.sf.oval.constraint.CheckWithCheck;
import com.aimprosoft.departments.models.Employee;
import com.aimprosoft.departments.services.EmployeeService;
import com.aimprosoft.departments.services.impl.EmployeeServiceImpl;

public class UniqueEmailCheck implements CheckWithCheck.SimpleCheck {

    private final EmployeeService employeeService;

    private UniqueEmailCheck() {
        this.employeeService = new EmployeeServiceImpl();
    }

    @Override
    public boolean isSatisfied(Object validatedObject, Object value) {
        String email = (String) value;
        Employee thatEmployee = employeeService.getEmployeeByEmail(email);
        Employee employee = (Employee) validatedObject;
        return thatEmployee == null || thatEmployee.getId().equals(employee.getId());
    }
}
