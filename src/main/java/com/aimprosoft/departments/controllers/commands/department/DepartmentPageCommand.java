package com.aimprosoft.departments.controllers.commands.department;

import com.aimprosoft.departments.models.Department;
import com.aimprosoft.departments.utils.WebUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DepartmentPageCommand extends DepartmentCommand {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer departmentId = WebUtil.parseIntParameterOrNull(req, "departmentId");
        Department department = departmentService.getDepartmentById(departmentId);
        req.setAttribute("department", department);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("departmentPage");
        requestDispatcher.forward(req, resp);
    }
}
