package com.aimprosoft.departments.utils.exceptions;

import java.util.Map;

public class ConstraintViolationException extends Exception {
    private final Map<String, String> constraintViolations;

    public ConstraintViolationException(Map<String, String> constraintViolations) {
        this.constraintViolations = constraintViolations;
    }

    public Map<String, String> getConstraintViolations(){
        return this.constraintViolations;
    }
}
