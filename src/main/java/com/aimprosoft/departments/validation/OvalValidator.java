package com.aimprosoft.departments.validation;


import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;
import net.sf.oval.context.FieldContext;
import net.sf.oval.context.OValContext;
import com.aimprosoft.departments.models.BaseEntity;
import com.aimprosoft.departments.utils.exceptions.ConstraintViolationException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OvalValidator {

    private final Validator validator = new Validator();

    public void validateEntity(BaseEntity baseEntity) throws ConstraintViolationException {
        List<ConstraintViolation> constraintViolations = validator.validate(baseEntity);
        if (constraintViolations.isEmpty()) return;
        Map<String, String> violationsMap = new HashMap<>();
        for (ConstraintViolation violation : constraintViolations){
            OValContext oValContext = violation.getContext();
            if (oValContext instanceof FieldContext){
                FieldContext fieldContext = (FieldContext) violation.getContext();
                String field = fieldContext.getField().getName();
                violationsMap.put(field, violation.getMessage());
            }
        }
        throw new ConstraintViolationException(violationsMap);
    }
}
