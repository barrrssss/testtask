package com.aimprosoft.departments.controllers.commands.employee;

import com.aimprosoft.departments.controllers.commands.department.DepartmentCommand;
import com.aimprosoft.departments.models.Department;
import com.aimprosoft.departments.models.Employee;
import com.aimprosoft.departments.services.EmployeeService;
import com.aimprosoft.departments.services.impl.EmployeeServiceImpl;
import com.aimprosoft.departments.utils.WebUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class GetDepartmentEmployeesCommand extends DepartmentCommand {

    private final EmployeeService employeeService;

    public GetDepartmentEmployeesCommand() {
        super();
        this.employeeService = new EmployeeServiceImpl();
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer departmentId = WebUtil.parseIntParameterOrNull(req, "departmentId");
        Department department = departmentService.getDepartmentById(departmentId);
        List<Employee> employees = employeeService.getDepartmentEmployees(departmentId);
        req.setAttribute("department", department);
        req.setAttribute("employees", employees);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("employeesPage");
        requestDispatcher.forward(req, resp);
    }
}
