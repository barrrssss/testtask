package com.aimprosoft.departments.controllers.commands.employee;

import com.aimprosoft.departments.utils.WebUtil;
import com.aimprosoft.departments.utils.exceptions.BadRequestException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EmployeePageCommand extends EmployeeCommand {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer employeeId = WebUtil.parseIntParameterOrNull(req, "employeeId");
        req.setAttribute("employee", employeeService.getEmployeeById(employeeId));
        Integer departmentId = WebUtil.parseIntParameterOrNull(req, "departmentId");
        if (departmentId == null) throw new BadRequestException("Invalid departmentId");
        req.setAttribute("departmentId", departmentId);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("employeePage");
        requestDispatcher.forward(req, resp);
    }
}
