<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
    <title>Employee</title>

    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>

<nav>
    <h3>
        <a href="/">Home</a>
        <a href="departments">Departments</a>
        <a href="departmentEmployees?departmentId=${requestScope.departmentId}">Employees</a>
    </h3>
</nav>

<hr>

<section>
    <header>
        <h2>${employee.id == null ? 'Create employee' : 'Edit employee'}</h2>
    </header>

    <form class="inputForm" method="post" action="saveEmployee">
        <input type="hidden" name="departmentId" value="${requestScope.departmentId}">
        <input type="hidden" name="employeeId" value="${employee.id}">

        <c:if test="${violations != null}">
            <jsp:useBean id="violations" type="java.util.Map" scope="request"/>
        </c:if>
        <div class="inputFormLabel">
            <label class="inputFormText" for="name">Name:</label>
            <input class="inputFormInput" id="name" type="text" value="<c:out value="${employee.name}"/>" name="name" required>

            <c:if test="${violations != null}">
                <label for="name" class="inputFormText warning"><c:out value="${violations.get('name')}"/></label>
            </c:if>
        </div>
        <div class="inputFormLabel">
            <label class="inputFormText" for="salary">Salary:</label>
            <input class="inputFormInput" id="salary" type="number" value="<c:out value="${employee.salary}"/>" name="salary" required>

            <c:if test="${violations != null}">
                <label for="salary" class="inputFormText warning"><c:out value="${violations.get('salary')}"/></label>
            </c:if>
        </div>
        <div class="inputFormLabel">
            <label class="inputFormText" for="birthDate">Birth date:</label>
            <input class="inputFormInput" id="birthDate" type="date" value="<c:out value="${employee.birthDate}"/>" name="birthDate" required>

            <c:if test="${violations != null}">
                <label for="birthDate" class="inputFormText warning"><c:out value="${violations.get('birthDate')}"/></label>
            </c:if>
        </div>
        <div class="inputFormLabel">
            <label class="inputFormText" for="email">Email:</label>
            <input class="inputFormInput" id="email" type="email" value="<c:out value="${employee.email}"/>" name="email" required>

            <c:if test="${violations != null}">
                <label for="email" class="inputFormInput warning"><c:out value="${violations.get('email')}"/></label>
            </c:if>
        </div>
        <button class="editButton" type="submit">Save</button>
        <a class="editButton" href="departmentEmployees?departmentId=${requestScope.departmentId}">Cancel</a>
    </form>
</section>

</body>
</html>
