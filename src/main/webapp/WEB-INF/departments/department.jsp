<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
    <title>Department</title>
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<nav>
    <h3>
        <a href="/">Home</a>
        <a href="departments">Departments</a>
    </h3>

</nav>

<hr>

<section>
    <header>
        <h2>
            ${department.id == null ? 'Create department' : 'Edit department'}
        </h2>
    </header>
    <form class="inputForm" method="post" action="saveDepartment">
        <input type="hidden" name="departmentId" value="${department.id}">
        <c:if test="${violations != null}">
            <jsp:useBean id="violations" type="java.util.Map" scope="request"/>
        </c:if>
        <div class="inputFormLabel">
            <label class="inputFormText" for="name">Name:</label>
            <input class="inputFormInput" type="text" id="name" value="<c:out value="${department.name}"/>" name="name"
                   required>
            <c:if test="${violations != null}">
                <label class="inputFormText warning" for="name"><c:out value="${violations.get('name')}"/></label>
            </c:if>
        </div>
        <button class="editButton" type="submit">Save</button>
        <a class="editButton" href="departments">Cancel</a>
    </form>
</section>
</body>
</html>
