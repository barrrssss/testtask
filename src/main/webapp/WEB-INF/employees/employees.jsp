<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
    <title>Employees</title>

    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>

<nav>
    <h3>
        <a href="/">Home</a>
        <a href="departments">Departments</a>
    </h3>

</nav>

<hr>

<section>

    <header>
        <h1><c:out value="${department == null ? 'Employees list' : department.name.concat(' employees list')}"/></h1>
    </header>

    <c:if test="${department != null}">
        <a class="editButton" href="employee?departmentId=${department.id}">Add employee</a>
    </c:if>
    <br><br>

    <table>
        <thead>
        <tr>
            <th>Name</th>
            <th>Salary</th>
            <th>Birth date</th>
            <th>Email</th>
            <th colspan="2">Actions</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${employees}" var="employee">
            <jsp:useBean id="employee" type="com.aimprosoft.departments.models.Employee"/>
            <tr>
                <td><c:out value="${employee.name}"/></td>
                <td class="number"><c:out value="${employee.salary}"/></td>
                <td><c:out value="${employee.birthDate}"/></td>
                <td><c:out value="${employee.email}"/></td>
                <td><a class="updateButton" href="employee?departmentId=${department.id}&employeeId=${employee.id}">Update</a></td>
                <td><a class="deleteButton" href="deleteEmployee?departmentId=${department.id}&employeeId=${employee.id}">Delete</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</section>

</body>
</html>

