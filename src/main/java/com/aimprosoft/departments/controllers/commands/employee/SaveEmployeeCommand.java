package com.aimprosoft.departments.controllers.commands.employee;

import com.aimprosoft.departments.utils.exceptions.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aimprosoft.departments.controllers.commands.Command;
import com.aimprosoft.departments.models.Employee;
import com.aimprosoft.departments.utils.WebUtil;
import com.aimprosoft.departments.utils.exceptions.ConstraintViolationException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

public class SaveEmployeeCommand extends EmployeeCommand implements Command {

    private final Logger logger = LoggerFactory.getLogger(SaveEmployeeCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer departmentId = WebUtil.parseIntParameterOrNull(request, "departmentId");
        if (departmentId == null) throw new BadRequestException("Invalid departmentId");
        request.setAttribute("departmentId", departmentId);
        Employee employee = createEmployeeFromRequestParameters(request);
        try {
            employeeService.saveEmployee(employee, departmentId);
            response.sendRedirect("departmentEmployees?departmentId=" + departmentId);
        } catch (ConstraintViolationException e){
            Map<String, String> violations = e.getConstraintViolations();
            logger.info("constraint violations {}", violations);
            request.setAttribute("violations", violations);
            request.setAttribute("employee", employee);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("employeePage");
            requestDispatcher.forward(request, response);
        }
    }

    private Employee createEmployeeFromRequestParameters(HttpServletRequest req){
        Integer employeeId = WebUtil.parseIntParameterOrNull(req, "employeeId");
        String name = req.getParameter("name");
        Integer salary = WebUtil.parseIntParameterOrNull(req, "salary");
        LocalDate parsedBirthDate = WebUtil.parseDateParameter(req, "birthDate");
        String email = req.getParameter("email");
        Employee employee = new Employee();
        employee.setId(employeeId);
        employee.setName(name);
        employee.setSalary(salary);
        employee.setBirthDate(parsedBirthDate);
        employee.setEmail(email);
        return employee;
    }
}
