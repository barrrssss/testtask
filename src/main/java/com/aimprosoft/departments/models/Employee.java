package com.aimprosoft.departments.models;

import net.sf.oval.constraint.*;
import com.aimprosoft.departments.validation.checks.UniqueEmailCheck;

import java.time.LocalDate;

public class Employee extends BaseEntity {

    @NotBlank(message = "cannot be blank")
    private String name;

    @Min(value = 0, message = "cannot be smaller than {min}")
    private Integer salary;

    private LocalDate birthDate;

    @Email(message = "is not a valid email")
    @CheckWith(value = UniqueEmailCheck.class, message = "there is same email")
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", birthDate=" + birthDate +
                ", email='" + email + '\'' +
                '}';
    }
}
