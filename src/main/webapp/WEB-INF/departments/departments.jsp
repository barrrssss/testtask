<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
    <title>Departments</title>

    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<nav>
    <h3><a href="/">Home</a></h3>
</nav>

<hr>

<section>

    <header>
        <h2>Departments list</h2>
    </header>

    <a class="editButton" href="department">Add department</a>

    <br><br>

    <table class="departmentTable">
        <thead>
        <tr>
            <th>Name</th>
            <th colspan="3">Actions</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${departments}" var="department">
            <jsp:useBean id="department" type="com.aimprosoft.departments.models.Department"/>
            <tr>
                <td><c:out value="${department.name}"/></td>
                <td><a class="getButton" href="departmentEmployees?departmentId=${department.id}">Employees</a></td>
                <td><a class="updateButton" href="department?departmentId=${department.id}">Update</a></td>
                <td><a class="deleteButton" href="deleteDepartment?departmentId=${department.id}">Delete</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</section>

</body>
</html>
