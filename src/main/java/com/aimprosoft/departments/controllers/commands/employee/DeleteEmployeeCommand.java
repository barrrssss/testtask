package com.aimprosoft.departments.controllers.commands.employee;

import com.aimprosoft.departments.utils.WebUtil;
import com.aimprosoft.departments.utils.exceptions.BadRequestException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteEmployeeCommand extends EmployeeCommand {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer employeeId = WebUtil.parseIntParameterOrNull(req, "employeeId");
        Integer departmentId = WebUtil.parseIntParameterOrNull(req, "departmentId");
        if (departmentId == null) throw new BadRequestException("Invalid departmentId");
        employeeService.deleteById(employeeId);
        resp.sendRedirect("departmentEmployees?departmentId=" + departmentId);
    }
}
