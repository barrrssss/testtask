package com.aimprosoft.departments.controllers.commands;

import com.aimprosoft.departments.controllers.commands.department.DeleteDepartmentCommand;
import com.aimprosoft.departments.controllers.commands.department.DepartmentPageCommand;
import com.aimprosoft.departments.controllers.commands.department.GetAllDepartmentsCommand;
import com.aimprosoft.departments.controllers.commands.department.SaveDepartmentCommand;
import com.aimprosoft.departments.controllers.commands.employee.DeleteEmployeeCommand;
import com.aimprosoft.departments.controllers.commands.employee.EmployeePageCommand;
import com.aimprosoft.departments.controllers.commands.employee.GetDepartmentEmployeesCommand;
import com.aimprosoft.departments.controllers.commands.employee.SaveEmployeeCommand;

import java.util.HashMap;
import java.util.Map;

public class CommandFactory {

    private final Map<String, Command> uriCommandMap = new HashMap<>();

    private final Command NOT_FOUND_COMMAND = new NotFoundCommand();

    {
        uriCommandMap.put("/", new MainPageCommand());
        uriCommandMap.put("/departments", new GetAllDepartmentsCommand());
        uriCommandMap.put("/department", new DepartmentPageCommand());
        uriCommandMap.put("/saveDepartment", new SaveDepartmentCommand());
        uriCommandMap.put("/deleteDepartment", new DeleteDepartmentCommand());

        uriCommandMap.put("/departmentEmployees", new GetDepartmentEmployeesCommand());
        uriCommandMap.put("/employee", new EmployeePageCommand());
        uriCommandMap.put("/saveEmployee", new SaveEmployeeCommand());
        uriCommandMap.put("/deleteEmployee", new DeleteEmployeeCommand());
    }

    public Command getCommand(String uri){
        return uriCommandMap.getOrDefault(uri, NOT_FOUND_COMMAND);
    }
}
