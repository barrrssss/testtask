package com.aimprosoft.departments.repositories;

import com.aimprosoft.departments.models.Department;

import java.util.List;

public interface DepartmentRepository {

    List<Department> getAllDepartments();

    Department save(Department department);

    Department getDepartmentById(Integer id);

    Department getDepartmentByName(String name);

    void deleteById(Integer id);
}
