package com.aimprosoft.departments.models;

import net.sf.oval.constraint.CheckWith;
import net.sf.oval.constraint.NotBlank;
import com.aimprosoft.departments.validation.checks.UniqueNameCheck;

public class Department extends BaseEntity {

    @NotBlank(message = "cannot be blank")
    @CheckWith(value = UniqueNameCheck.class, message = "there is same name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                '}';
    }
}
