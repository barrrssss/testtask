package com.aimprosoft.departments.controllers.commands.department;

import com.aimprosoft.departments.controllers.commands.Command;
import com.aimprosoft.departments.services.DepartmentService;
import com.aimprosoft.departments.services.impl.DepartmentServiceImpl;

public abstract class DepartmentCommand implements Command {

    protected final DepartmentService departmentService;

    public DepartmentCommand() {
        this.departmentService = new DepartmentServiceImpl();
    }

}
