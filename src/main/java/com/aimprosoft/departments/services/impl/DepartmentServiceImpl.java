package com.aimprosoft.departments.services.impl;

import com.aimprosoft.departments.utils.exceptions.ConstraintViolationException;
import com.aimprosoft.departments.validation.OvalValidator;
import com.aimprosoft.departments.models.Department;
import com.aimprosoft.departments.repositories.DepartmentRepository;
import com.aimprosoft.departments.repositories.impl.JdbcDepartmentRepository;
import com.aimprosoft.departments.services.DepartmentService;

import java.util.List;

public class DepartmentServiceImpl implements DepartmentService {

    private final OvalValidator ovalValidator;

    private final DepartmentRepository departmentRepository;

    public DepartmentServiceImpl() {
        this.departmentRepository = new JdbcDepartmentRepository();
        this.ovalValidator = new OvalValidator();
    }

    @Override
    public List<Department> getAllDepartments() {
        return departmentRepository.getAllDepartments();
    }

    @Override
    public void saveDepartment(Department department) throws ConstraintViolationException {
        ovalValidator.validateEntity(department);
        departmentRepository.save(department);
    }

    @Override
    public Department getDepartmentById(Integer id) {
        if (id == null) return null;
        return departmentRepository.getDepartmentById(id);
    }

    @Override
    public void deleteById(Integer id) {
        if (id != null) {
            departmentRepository.deleteById(id);
        }
    }

    @Override
    public Department getDepartmentByName(String name) {
        return departmentRepository.getDepartmentByName(name);
    }
}
