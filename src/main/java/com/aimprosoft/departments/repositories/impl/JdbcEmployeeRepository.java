package com.aimprosoft.departments.repositories.impl;

import com.aimprosoft.departments.utils.exceptions.RuntimeSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aimprosoft.departments.models.Employee;
import com.aimprosoft.departments.repositories.EmployeeRepository;
import com.aimprosoft.departments.utils.DataBaseUtil;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcEmployeeRepository implements EmployeeRepository {

    private final DataSource dataSource;

    public static final Logger logger = LoggerFactory.getLogger(JdbcDepartmentRepository.class);

    private static final String GET_ALL_QUERY = "SELECT * FROM employees WHERE department_id = ?";
    private static final String CREATE_QUERY = "INSERT INTO employees(name, salary, birth_date, email, department_id) VALUES(?, ?, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE employees SET name = ?, salary = ?, birth_date = ?, email = ? WHERE id = ?";
    private static final String GET_BY_ID_QUERY = "SELECT * FROM employees WHERE id = ?";
    private static final String GET_BY_EMAIL_QUERY = "SELECT * FROM employees WHERE email = ?";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM employees WHERE id = ?";

    public JdbcEmployeeRepository() {
        this.dataSource = DataBaseUtil.getDataSource();
    }

    @Override
    public List<Employee> getDepartmentEmployees(Integer departmentId) {
        logger.debug("getDepartmentEmployees {}", departmentId);
        List<Employee> list = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_QUERY)) {
            preparedStatement.setInt(1, departmentId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Employee employee = createEmployee(resultSet);
                list.add(employee);
            }
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
        return list;
    }

    @Override
    public Employee saveEmployee(Employee employee, Integer departmentId) {
        if (employee.getId() == null) {
            return create(employee, departmentId);
        } else {
            return update(employee);
        }
    }

    private Employee create(Employee employee, Integer departmentId) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, employee.getName());
            preparedStatement.setInt(2, employee.getSalary());
            preparedStatement.setDate(3, Date.valueOf(employee.getBirthDate()));
            preparedStatement.setString(4, employee.getEmail());
            preparedStatement.setInt(5, departmentId);
            preparedStatement.executeUpdate();
            ResultSet key = preparedStatement.getGeneratedKeys();
            if (!key.next()) return employee;
            Integer id = key.getInt(1);
            employee.setId(id);
        } catch (SQLException e){
            throw new RuntimeSQLException(e);
        }
        return employee;
    }

    private Employee update(Employee employee) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, employee.getName());
            preparedStatement.setInt(2, employee.getSalary());
            preparedStatement.setDate(3, Date.valueOf(employee.getBirthDate()));
            preparedStatement.setString(4, employee.getEmail());
            preparedStatement.setInt(5, employee.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new RuntimeSQLException(e);
        }
        return employee;
    }

    @Override
    public Employee getEmployeeById(Integer id) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID_QUERY)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            return createEmployee(resultSet);
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
    }

    @Override
    public Employee getEmployeeByEmail(String email) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_EMAIL_QUERY)) {
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            return createEmployee(resultSet);
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID_QUERY)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
    }

    private Employee createEmployee(ResultSet resultSet) throws SQLException {
        Employee employee = new Employee();
        employee.setId(resultSet.getInt("id"));
        employee.setName(resultSet.getString("name"));
        employee.setSalary(resultSet.getInt("salary"));
        employee.setBirthDate(resultSet.getDate("birth_date").toLocalDate());
        employee.setEmail(resultSet.getString("email"));
        return employee;
    }
}
