package com.aimprosoft.departments.utils;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;

public class DataBaseUtil {

    private static final MysqlDataSource dataSource = new MysqlDataSource();

    private static final String URL = "jdbc:mysql://localhost:3306/testTask?serverTimezone=UTC&useUnicode=yes&characterEncoding=UTF-8";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        dataSource.setURL(URL);
        dataSource.setUser(USER);
        dataSource.setPassword(PASSWORD);
    }

    public static DataSource getDataSource(){
        return dataSource;
    }


}
