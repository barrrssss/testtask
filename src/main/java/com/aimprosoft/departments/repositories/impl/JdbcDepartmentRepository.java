package com.aimprosoft.departments.repositories.impl;

import com.aimprosoft.departments.utils.exceptions.RuntimeSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aimprosoft.departments.models.Department;
import com.aimprosoft.departments.repositories.DepartmentRepository;
import com.aimprosoft.departments.utils.DataBaseUtil;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcDepartmentRepository implements DepartmentRepository {

    private final DataSource dataSource;

    private static final Logger logger = LoggerFactory.getLogger(JdbcDepartmentRepository.class);

    private static final String GET_ALL_QUERY = "SELECT * FROM departments";
    private static final String CREATE_QUERY = "INSERT INTO departments(name) VALUES(?)";
    private static final String UPDATE_QUERY = "UPDATE departments SET name = ? WHERE id = ?";
    private static final String GET_BY_ID_QUERY = "SELECT * FROM departments WHERE id = ?";
    private static final String GET_BY_NAME_QUERY = "SELECT * FROM departments WHERE name = ?";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM departments WHERE id = ?";

    public JdbcDepartmentRepository() {
        this.dataSource = DataBaseUtil.getDataSource();
    }

    @Override
    public List<Department> getAllDepartments() {
        List<Department> list = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_QUERY)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Department department = new Department();
                department.setId(resultSet.getInt("id"));
                department.setName(resultSet.getString("name"));
                list.add(department);
            }
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
        return list;
    }

    @Override
    public Department save(Department department) {
        logger.debug("save {}", department);
        if (department.getId() == null) {
            return create(department);
        } else {
            return update(department);
        }
    }

    private Department create(Department department) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, department.getName());
            preparedStatement.executeUpdate();
            ResultSet key = preparedStatement.getGeneratedKeys();
            if (!key.next()) return department;
            Integer id = key.getInt(1);
            department.setId(id);
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
        return department;
    }

    private Department update(Department department) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, department.getName());
            preparedStatement.setInt(2, department.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new RuntimeSQLException(e);
        }
        return department;
    }

    @Override
    public Department getDepartmentById(Integer id) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID_QUERY)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            Department department = new Department();
            department.setId(id);
            department.setName(resultSet.getString("name"));
            return department;
        } catch (SQLException e) {
            logger.warn(e.getMessage());
            throw new RuntimeSQLException(e);
        }
    }

    @Override
    public Department getDepartmentByName(String name) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_NAME_QUERY)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            Department department = new Department();
            department.setId(resultSet.getInt("id"));
            department.setName(resultSet.getString("name"));
            return department;
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID_QUERY)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
    }
}
